# József Jancsó
#### Hungary,2621
##### +36309882888
@jancsoj78@gmail.com
| Roles | Description |
| - | -|
| DevOps | Senior Technical Support for Developer Teams on Application level |
| SysOps | Senior Technical Support for Developer Teams on OS level|
| Project Management | Full Organize Technical Projects |
| Remote Team Leader| [Remote] Leading Technical Teams [Effectively] |
| System Analyst | Loganalyzing on OS and Software layers |
| Logging, Monitoring | ELK, Dynatrace,Splunk, Icinga2, Grafana,[node.js] |
| CI/CD | Gitlab CI/CD ,Openshift, Jenkins |

| Platforms | Experience |
|- | - |
| Cloud | AWS,DevOps tools,Azure,Openshift |
| Hybrid | [plugins/github/README.md][PlGh] |
| On Promisse | [plugins/github/README.md][PlGh] |

| Technologies | Experience |
|- | - |
| Virtualization | AWS,Azure,Virtualbox,VmWare,Hyper-V |
| Containers | Docker, Openshift, Heroku|
| OS Independency| Centos,Debian,Windows-servers, Android, CoreOS,Atomic,Alpine|

| CV  | CV on Google Drive downloadable formats: docx,pdf |
| - | -|
| Hungarian CV | last update 2021 [hu]|
| English CV | last update 2021 [eng] |

- MindSets:
[![N|Small](https://gitlab.com/Jancsoj78/cv/-/raw/master/devops.jpg)](https://gitlab.com/Jancsoj78/cv/-/raw/master/devops.jpg)
- Git Flows
[![N|Small](https://gitlab.com/Jancsoj78/cv/-/raw/master/gitflow.png)](https://gitlab.com/Jancsoj78/cv/-/raw/master/gitflow.png)

[Effectively]:<https://otthoniroda.com>
[Remote]: <https://otthoniroda.com>
